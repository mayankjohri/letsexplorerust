# CMake generated Testfile for 
# Source directory: /Users/mayank.johri/.cargo/registry/src/github.com-1ecc6299db9ec823/shaderc-sys-0.6.2/build/glslang
# Build directory: /Users/mayank.johri/code/mj/books/ler/book_1_lets_explore_rust/14 - GUI/basalt/basic/target/debug/build/shaderc-sys-c8a45c39ad293755/out/build/glslang
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(glslang-testsuite "bash" "runtests" "/Users/mayank.johri/code/mj/books/ler/book_1_lets_explore_rust/14 - GUI/basalt/basic/target/debug/build/shaderc-sys-c8a45c39ad293755/out/build/glslang/localResults" "/Users/mayank.johri/code/mj/books/ler/book_1_lets_explore_rust/14 - GUI/basalt/basic/target/debug/build/shaderc-sys-c8a45c39ad293755/out/build/glslang/StandAlone/glslangValidator" "/Users/mayank.johri/code/mj/books/ler/book_1_lets_explore_rust/14 - GUI/basalt/basic/target/debug/build/shaderc-sys-c8a45c39ad293755/out/build/glslang/StandAlone/spirv-remap")
set_tests_properties(glslang-testsuite PROPERTIES  WORKING_DIRECTORY "/Users/mayank.johri/.cargo/registry/src/github.com-1ecc6299db9ec823/shaderc-sys-0.6.2/build/glslang/Test/" _BACKTRACE_TRIPLES "/Users/mayank.johri/.cargo/registry/src/github.com-1ecc6299db9ec823/shaderc-sys-0.6.2/build/glslang/CMakeLists.txt;223;add_test;/Users/mayank.johri/.cargo/registry/src/github.com-1ecc6299db9ec823/shaderc-sys-0.6.2/build/glslang/CMakeLists.txt;0;")
subdirs("External")
subdirs("glslang")
subdirs("OGLCompilersDLL")
subdirs("StandAlone")
subdirs("SPIRV")
subdirs("hlsl")
subdirs("gtests")
